
void init(char *,char *);
void dispName();
void upperCaseFirstLetter();
void dealloc();

int main()
{
	char fName[] = "Lajos";
	char lName[]= "nAgy";
	init(fName, lName);
	dispName();
	upperCaseFirstLetter();
	dispName();
	dealloc();
	return 0;
}