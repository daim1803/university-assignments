#include <iostream>
#include <string>

using namespace std;

class nevek {
private:
	char *firstName;
	char *lastName;
public:
	nevek(char *k, char *cs);
	void dispName();
	~nevek() {
		delete[] firstName;
		delete[] lastName;
	};
	void upperCaseFirstLetter();
};


nevek::nevek(char *k, char *cs) {
	firstName = new char[strlen(k) + 1];
	strcpy(firstName, k);
	lastName = new char[strlen(cs) + 1];
	strcpy(lastName, cs);
}

void nevek::dispName()
{
	int n = strlen(lastName);
	for (int i = 0; i < n; i++)
		cout << lastName[i];
	cout << " ";
	n = strlen(firstName);
	for (int i = 0; i < n; i++)
		cout << firstName[i];
	cout << endl;
}


void nevek::upperCaseFirstLetter()
{
	if (firstName[0] >= 'a' && firstName[0] <= 'z')
	{
		firstName[0] -= 'a';
		firstName[0] += 'A';
	}
	int n = strlen(firstName);
	for (int i = 1; i < n; i++)
	{
		if (firstName[i] >= 'A' && firstName[i] <= 'A')
			firstName[i] -= 'A' + 'a';
	}
	if (lastName[0] >= 'a' && lastName[0] <= 'z')
	{
		lastName[0] -= 'a';
		lastName[0] += 'A';
	}
	n = strlen(lastName);
	for (int i = 1; i < n; i++)
	{
		if (lastName[i] >= 'A' && lastName[i] <= 'Z')
		{
			lastName[i] -= 'A';
			lastName[i] += 'a';
		}
	}
}

int main()
{
	char s[] = "Adam";
	char p[] = "kISs";
	nevek v1(s,p);
	v1.dispName();
	v1.upperCaseFirstLetter();
	v1.dispName();
	return 0;
}