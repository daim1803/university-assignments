# Laboratory 2

## 1.

## 2.

### a)

    Create a module for handling names (first and lastname) use the following methods:

        - init - initialize the names
        - dealloc - deallocate the char vectors where the names are stored
        - upperCaseFirstLetter - changes the first character to uppercase, the others to lowercase
        - display - displays the full name

    Write a main program which uses the module above (the main must be in a seperate file).
    Make sure the static variables are safe

    You can use the following functions from the cctype library

    int toupper(int aChar);
    int tolower(int aChar);

### b)
    Solve the problem above with:

    b1) namespaces
    b2) classes