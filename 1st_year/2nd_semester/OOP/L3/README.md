# Laboratory 3

## 1.

    Create a class named Point to represent a 2D point.
    Then create a class named Circle with a Point data member for the center of the circle and a real type to represent the radius of the circle. Write functions to calculate the area and circumference of a circle and to return a random Point in the circle.

## 2.