#include <iostream>
#include <ctime>
#include <math.h>

using namespace std;

const float pi = 3.14;

class Point {
	float x;
	float y;
public:
	Point(float x, float y);
	float getX() { return x; }
	float getY() { return y; }
};

Point::Point(float x, float y) : x(x), y(y) {}

class Circle {

	Point centerPoint;
	float radius;
public:
	Circle(float x, float b, float r) :
		centerPoint(x, b), radius(r) {}
	void display();
	float calcArea(); // radius^2 * pi
	float calcCircumference();
	void getRandomPoint();
};

float Circle::calcCircumference() {

	return 2 * this->radius * pi;
}

float Circle::calcArea() {
	return this->radius * this->radius * pi;
}

void Circle::display()
{
	cout << "X = " << centerPoint.getX() << "Y = " << centerPoint.getY();
	cout << "r = " << this->radius;
}

void Circle::getRandomPoint()
{
	float newA = (rand() % int(2.0f * pi));
	cout << newA << endl; //<< cos(newA);
	float x = (float)this->radius * cos(newA);
	float y = (float)this->radius * sin(newA);
	cout << "Point in the circle: ";
	cout << "X = " << x << " Y= " << y;
}

int main()
{
	float x, y, r;
	cout << "x = ";
	cin >> x;
	cout << "y = ";
	cin >> y;
	cout << "r = ";
	cin >> r;
	srand(time(NULL));
	Circle myCircle(x, y, r);

	cout << endl << "Area of the circle: " << myCircle.calcArea() << endl;
	cout << "Circumference of the circle: " << myCircle.calcCircumference() << endl;
	myCircle.getRandomPoint();
}
