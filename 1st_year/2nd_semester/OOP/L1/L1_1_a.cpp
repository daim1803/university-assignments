// DACZO ALPAR - 511 - daim1803
#include <iostream>
#include <ctime>

using namespace std;

int randomNumber(int a, int b)
{
	srand(time(NULL));
	int szam = rand() % (b - a);
	szam += a;
	return szam;
}

void game(int generatedNumber)
{
	int guessedNumber;
	cout << "Guess the number!" << endl;
	do {
		cin >> guessedNumber;
		if (guessedNumber == generatedNumber) cout << "Congrats!" << endl;
		else if (guessedNumber < generatedNumber) cout << "Greater than!" << endl;
		else if (guessedNumber > generatedNumber) cout << "Less than!" << endl;
	} while (guessedNumber != generatedNumber);
}

int main() {
	game(randomNumber(1, 1000));
	return 0;
}
