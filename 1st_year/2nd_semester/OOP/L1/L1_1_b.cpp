// DACZO ALPAR - 511 - daim1803
#include <iostream>
#include <fstream>
using namespace std;

void spiralMatrix(const int *matrix, int n, int *spiral)
{
	int i = -1;
	int step = 0;
	int counter = 0;
	int border = n;

	// Until we didn't copied all of the elements from matrix to spiral
	while (counter < n*n)
	{
		// Moving right
		while (step < border && counter <n*n)
		{

			i++;
			spiral[counter] = matrix[i];
			step++;
			counter++;
		}
		border--;

		step = 0;
		// Moving down
		while (step < border && counter < n*n)
		{

			i += n;
			spiral[counter] = matrix[i];
			counter++;
			step++;
		}
		step = 0;

		// Moving left
		while (step < border  && counter < n*n)
		{
			i--;
			spiral[counter] = matrix[i];
			counter++;
			step++;
		}
		step = 0;
		border--;

		// Moving up
		while (step < border && counter < n*n)
		{
			i -= n;
			spiral[counter] = matrix[i];
			counter++;
			step++;
		}
		step = 0;
	}
}

void displayMatrix(const int n, const int *a)
{
	int k = 0;
	for (int i = 0; i < n*n; i++)
	{
		cout << a[i] << " ";
		k++;
		if (k == n) {
			cout << endl;
			k = 0;
		}
	}
	cout << endl;
}


void displayVector(const int n, const int *a)
{
	for (int i = 0; i < n; i++)
		cout << a[i] << " ";
	cout << endl;
}

int main()
{
	ifstream f("input.txt");

	// Making sure the file is opened
	if (f.fail())
	{
		cout << "An error has occured with the input file!" << endl;
		return 1;
	}
	// Reading size of the matrix

	int n;
	f >> n;

	// Allocating the memory for the matrix
	int *matrix = new int[n*n];
	int *transformedMatrix = new int[n*n];

	// Reading the matrix elements
	for (int i = 0; i < n*n; i++)
	{
		f >> matrix[i];
	}

	// Displaying the matrix before the transformation
	displayMatrix(n, matrix);

	spiralMatrix(matrix, n, transformedMatrix);

	// Displaying the matrix after the transformation
	displayMatrix(n, transformedMatrix);

	// deallocating memory
	delete []matrix;
	delete []transformedMatrix;
	return 0;
}