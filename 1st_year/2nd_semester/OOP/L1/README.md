# Laboratory 1

Based on a sequence number you have to solve one of the exercises(nr 1. or nr 2.)

## 1.

### a)
    Write a program, which generates a random number between [1,100], then asks the user to write a number.
    If the number equals with the randomly generated number, then the program displays "Congrats!".
    Else it will display if the random number is greater or less than the number and ask for another input.

    You have to create the following functions for the solution:

    int randomNumber(int a, int b);

        - genearates a random number between [a,b]

    void game(int generatedNumber);

        - manages the number guessing game, the user tries to guess the generatedNumber, if the guess equals the generatedNumber displays "Congrats!", else "Greater than!" or "Less than!" according to the guessed number compared to the generatedNumber.




### b)

    Display a square matrix in a spiral form starting from the left upper element going to the right. The matrix will be read from a file.

    The function which should make the transformation of the matrix:

    void spiralMatrix(const int* matrix, int n, int * spiral);
    where:
        - const int* matrix - input matrix stored as a vector
        - int n, the number of elements the matrix has
        - int *spiral - the transformed matrix

## 2.

###