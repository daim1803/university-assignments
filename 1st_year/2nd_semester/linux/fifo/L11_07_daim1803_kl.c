// Daczo Alpar - daim1803 - 531
// A kliens információt kér állományokról. 
// A szerver ellenőrzi, hogy létezik-e az adott állomány, és válaszként visszaküldi az állomány típusát, méretét, 
// hány különböző néven hivatkoznak rá, szöveges állományok esetében pedig az állomány sorainak és szavainak számát is.
// A kliens az állományneveket paraméterként kapja.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "common.h"

#define SERVER_FIFO_NAME "sz_daim1803"

int main(int argc, char* argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Error: No arguments given\n");
        fprintf(stderr, "Usage: ./kl filename1 [filname2 filename3 ...]\n");
        return 1;
    }

    int serverFd, clientFd;
    Request request;
    Response response;

    // Create the client FIFO
    request.pidNum = getpid();
    char clientFifoName[MAX_BUFFER_SIZE];
    sprintf(clientFifoName, "kl_daim1803_%d", request.pidNum);
    mkfifo(clientFifoName, 0666);

    // Open the server FIFO for writing requests
    serverFd = open(SERVER_FIFO_NAME, O_WRONLY);
    if (serverFd == -1) {
        fprintf(stderr, "[Client] Failed to open server FIFO\n");
        return 1;
    }

    for (int i = 1; i < argc; i++) {
        // Write the request to the server FIFO
        strcpy(request.filename, argv[i]);
        if (write(serverFd, &request, sizeof(Request)) == -1) {
            fprintf(stderr, "[Client] Failed to write request to server FIFO\n");
            return 1;
        }

        // If we sent stop to the server we don't except an answer
        if (strcmp(argv[i], "stop") == 0) {
            break;
        }

        // Open the client FIFO for reading the response
        clientFd = open(clientFifoName, O_RDONLY);
        if (clientFd == -1) {
            fprintf(stderr, "[Client] Failed to open client FIFO\n");
            return 1;
        }

        // Read the response from the client FIFO
        if (read(clientFd, &response, sizeof(Response)) > 0) {
            if (response.info_to_display_count > 1) {
                printf("[Client] Response for file: %s\n", argv[i]);
                printf("[Client] Type: %s\n", response.type);
                printf("[Client] Size: %d\n", response.size);
                printf("[Client] Link count: %d\n", response.link_count);
                if (response.info_to_display_count > 3) {
                    printf("[Client] Line count: %d\n", response.line_count);
                    printf("[Client] Word count: %d\n", response.word_count);
                }
            } else {
                printf("[Client] %s\n", response.type);
            }
            printf("\n");
        }
    }
    // Close and delete the client FIFO
    close(clientFd);
    unlink(clientFifoName);

    // Close the server FIFO
    close(serverFd);

    return 0;
}