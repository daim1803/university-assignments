// Daczo Alpar - daim1803 - 531
// A kliens információt kér állományokról. 
// A szerver ellenőrzi, hogy létezik-e az adott állomány, és válaszként visszaküldi az állomány típusát, méretét, 
// hány különböző néven hivatkoznak rá, szöveges állományok esetében pedig az állomány sorainak és szavainak számát is.
// A kliens az állományneveket paraméterként kapja.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "common.h"

#define SERVER_FIFO_NAME "sz_daim1803"

int processRequest(const Request* request, Response* response) {
    char command[MAX_BUFFER_SIZE];
    sprintf(command, "./L11_07_daim1803.sh %s", request->filename);

    FILE *pipe = popen(command, "r");

    if (pipe == NULL)
    {
        perror("popen");
        return 1;
    }

    char msg[MAX_BUFFER_SIZE];
    int iterator = 0;
    while (fgets(msg, sizeof(msg), pipe) != NULL)
    {
        msg[strcspn(msg, "\n")] = '\0';
        switch (iterator)
        {
        case 0:
            /* code */
            strcpy(response->type, msg);
            break;
        case 1:
            response->size=atoi(msg);
            break;
        case 2:
            response->link_count=atoi(msg);
            break;
        case 3:
            response->line_count=atoi(msg);
            break;
        case 4: 
            response->word_count=atoi(msg);
        default:
            break;
        }
        iterator++;
    }
    pclose(pipe);
    response->info_to_display_count=iterator;
    return 0;
}

void handleClient(const Request* request) {
    Response response;
    int pidNum = getpid();
    // Process the request
    char clientFifoName[MAX_BUFFER_SIZE];
    sprintf(clientFifoName, "kl_daim1803_%d", request->pidNum);
    // Open the client FIFO for writing the response
    int clientFd = open(clientFifoName, O_WRONLY);
    if (clientFd == -1) {
        fprintf(stderr, "[Server]:%d Failed to open client FIFO: %s\n", pidNum, clientFifoName);
    }

    int result = processRequest(request, &response);
    if (result != 0) {
        fprintf(stderr, "[Server]:%d Failed to process the request!\n", pidNum);
        exit(1);
    }
            
    // Write the response to the client FIFO
    if (write(clientFd, &response, sizeof(Response)) == -1) {
        fprintf(stderr, "[Server]:%d Failed to write response to client FIFO\n", pidNum);
    }
    printf("[Server]:%d The server sends response about the file: %s\n", pidNum, request->filename);
    
    // Close the client FIFO
    close(clientFd);
}

int main() {
    int serverFd;
    Request request;

    // Create the server FIFO
    mkfifo(SERVER_FIFO_NAME, 0666);
    int pidNum = getpid();

    // Open the server FIFO for reading
    serverFd = open(SERVER_FIFO_NAME, O_RDONLY);
    if (serverFd == -1) {
        fprintf(stderr, "Failed to open server FIFO\n");
        return 1;
    }
    
    while (1) {
        // Read a request from the client FIFO
        if (read(serverFd, &request, sizeof(Request)) > 0) {
            printf("[Server]:%d Received request: %s\n",pidNum ,request.filename);
            if (strcmp("stop",request.filename) == 0) {
                break;
            }

            pid_t pid = fork();
            if (pid == 0) {
                // Gyermekfolyamat
                handleClient(&request);
                return 0;
            } else if (pid > 0) {
                continue;
            } else {
                // Fork hiba
                fprintf(stderr, "[Server]:%d Fork error\n", pidNum);
                exit(1);
            }
        }
    }

    // Close and delete the server FIFO
    close(serverFd);
    unlink(SERVER_FIFO_NAME);
    printf("[Server]:%d The server is closing...\n", pidNum);

    return 0;
}