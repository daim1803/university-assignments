#!/bin/bash
# Daczo Alpar - daim1803 - 531

filename=$1

# Ellenőrizzük, hogy a fájl létezik-e
if [ -e "$filename" ]; then
    # Fájltípus meghatározása
    file_type=$(file -b --mime-type "$filename")
    echo "$file_type"

    # Fájlméret meghatározása
    file_size=$(stat -c%s "$filename")
    echo "$file_size"

    # Hivatkozások száma
    link_count=$(stat -c%h "$filename")
    echo "$link_count"

    # Szöveges fájl esetén sorok és szavak száma
    if [ -f "$filename" ]; then
        if [ -n "$(file -b --mime-type "$filename" | grep -i "text")" ]; then
            line_count=$(wc -l < "$filename")
            echo "$line_count"

            word_count=$(wc -w < "$filename")
            echo "$word_count"
        fi
    fi

else
    echo "$filename file not found"
fi
