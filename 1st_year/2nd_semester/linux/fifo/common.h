// common.h
#ifndef COMMON_H
#define COMMON_H

#define MAX_BUFFER_SIZE 256

typedef struct {
    int pidNum;
    char filename[MAX_BUFFER_SIZE - 23];
} Request;

typedef struct {
    char type[MAX_BUFFER_SIZE];
    int size;
    int link_count;
    int line_count;
    int word_count;
    int info_to_display_count;
} Response;

#endif

