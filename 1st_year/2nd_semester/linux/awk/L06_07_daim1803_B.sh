#!/bin/bash
# Daczo Alpar - daim1803 - 531
# Asszociatív tömb segítségével határozzuk meg, hogy ebben a hónapban, a hét melyik napján (hétfő, kedd, stb.) hányan voltak bejelentkezve,
# illetve határozzuk meg, hogy a hét melyik napján történt a legtöbb bejelentkezés.
# Megj.: hagyatkozhatunk arra, hogy az egyetem szerverén a last parancs eleve a hónap elejétől kezdve listázza a bejelentkezéseket
# (mivel az ezeket nyilvántartó /var/log/wtmp állomány havonta felülíródik).

last | head -n-2 | tr -s ' ' > tmp_file

awk '{
    days[$5 $6] += 1;
} END {
    max=0;
    maxday;
    for (day in days) {
        printf("%s: %d\n",day, days[day]);
        if (days[day] > max) {
            max = days[day];
            maxday = day;
        }
    }
    if (max != 0) {
        printf("A %s nap %d-szer jelentkeztek be, ami a legtobb a honapban\n", maxday, max);
    }
    else {
        printf("Egyiknap sem jelentkezett be senki");
    }
}' tmp_file

rm tmp_file

exit 0