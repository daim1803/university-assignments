#!/bin/bash
# Daczo Alpar - daim1803 - 531
# A) Adott egy állomány egy vizsga eredményeivel. Formátuma:
#        vezetéknév keresztnév felhasználónév média
# Ezt az állományt használva bemenetként, írjunk egy levelet minden vizsgázónak. Annak, aki átmenő jegyet kapott, ezt írjuk:
#         Kedves [vezetéknév] [keresztnév]!
#         A(z) [tantárgy] vizsgán elért osztályzata [média].
# Aki nem ment át, annak pedig azt írjuk, hogy:
#         Kedves [vezetéknév] [keresztnév]!
#         Jöjjön legközelebb is a(z) [tantárgy] tangyárgyból vizsgázni.
# ahol a [vezetéknév], [keresztnév], [tantárgy], [média] helykitöltőket a megfelelő adatokkal helyettesítjük,
# illetve a [tantárgy] a paraméterként van megadva, a többi információt pedig a bemeneti állományból kapjuk.
# A könnyebb ellenőrizhetőség kedvéért az elküldött szövegeket az aktuális rendszeridő kíséretében írjuk bele egy [tantárgy]_log nevű állományba is,
# ahol a [tantárgy] helykitöltőt szintén behelyettesítjük a tantárgy nevével. Példa a program futtatására: ./L06_07_A.sh oprendszerek eredmenyek.txt

# Ellenőrizzük a paraméterek számát
if [ $# -ne 2 ]; then
  echo "Error: The number of arguments must be 2!"
  echo "Usage: ./L05_03_daim1803.sh subject result_file"
  exit 1
fi

file=$2

# Ellenorizzuk, hogy az eredmeny file letezik
if [ ! -e "$file" ]; then
  echo "Error: $file does not exist!"
  exit 1
fi

# Ellenorizzuk, hogy az eredmeny file valoban file
if [ ! -f "$file" ]; then
  echo "Error: $file is not a file!"
  exit 1
fi

# Ellenorizzuk, hogy az eredmeny file olvashato
if [ ! -r "$file" ]; then
    echo "Warning: $file is not readable"
    exit 1
fi

subject=$1
output=$1"_log"
awk -v subject=$subject -v newline="\n" '{
if ($4 >= 5.0) {

    system("echo Kedves "$1" "$2"! > tmp_file")
    system("echo Az "subject" vizsgán elért osztályzata "$4" | cat >> tmp_file")
    system("cat tmp_file | write "$3)

    printf("Kedves %s %s!\n",$1,$2);
    printf("A(z) %s vizsgán elért osztályzata %f.\n",subject, $4);
}
else {

    system("echo Kedves "$1" "$2"! > tmp_file")
    system("echo Jöjjön legközelebb is az "subject" tangyárgyból vizsgázni. | cat >> tmp_file")
    system("cat tmp_file | write "$3)

    printf("Kedves %s %s!\n",$1,$2);
    printf("Jöjjön legközelebb is a(z) %s tangyárgyból vizsgázni.\n", subject);
}
}' "$file" > $output

rm tmp_file

exit 0
