#!/bin/bash
# Daczo Alpar - daim1803 - 531
# A paraméterekként megadott állományok első 30 sorából törölje ki azokat a sorokat,
# amelyek az első paraméterként megadott szót tartalmazzák. A paraméterek sorrendje: szó állományn(év/evek).

# Ellenőrizzük, hogy legalább két argumentumot megadtak-e
if [ $# -lt 2 ]; then
  echo "Error: Please add the word and atleast one filename as an argument!"
  echo "Usage: ./L05_03_daim1803.sh word filename1 [filenames]"
  exit 1
fi

word="$1"

shift
for file in "$@"; do
  # Ellenőrizzük, hogy az állomány létezik-e
  if [ ! -e "$file" ]; then
    echo "Warning: $file does not exist"
    continue
  fi

  # Ellenőrizzük, hogy az állomány valóban állomány-e
  if [ ! -f "$file" ]; then
    echo "Warning: $file is not a file"
    continue
  fi

  # Ellenőrizzük, hogy olvasható-e az állomány
  if [ ! -r "$file" ]; then
    echo "Warning: $file is not readable"
    continue
  fi

  # Töröljük az első 30 sorból azokat a sorokat, amelyek tartalmazzák a szót
  sed -i "1,30 {/$word/d;}" "${file}"

  echo "Az allomany $file modositva lett."
done

exit 0
