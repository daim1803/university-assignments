#!/bin/bash
# Daczo Alpar - daim1803 - 531
# A paraméterekként megadott állományokban minden magánhangzót változtasson meg,
# a kisbetűt nagyra, a nagyot kicsire.

# Ellenőrizzük, hogy legalább egy állományt megadtak-e
if [ $# -lt 1 ]; then
  echo "Error: Please add atleast one filename as an argument!"
  echo "Usage: ./L05_07_daim1803.sh filename1 [filenames]"
  exit 1
fi

for file in "$@"; do
  # Ellenőrizzük, hogy az állomány létezik-e
  if [ ! -e "$file" ]; then
    echo "Warning: $file does not exist"
    continue
  fi

  # Ellenőrizzük, hogy az állomány valóban állomány-e
  if [ ! -f "$file" ]; then
    echo "Warning: $file is not a file"
    continue
  fi

  # Ellenőrizzük, hogy olvasható-e az állomány
  if [ ! -r "$file" ]; then
    echo "Warning: $file is not readable"
    continue
  fi

  # Cseréljük ki az összes magánhangzót kis- és nagybetű között
  sed -i -e 'y/aeiouáéíóöőúüűAEIOUÁÉÍÓÖŐÚÜŰ/AEIOUÁÉÍÓÖŐÚÜŰaeiouáéíóöőúüű/' "$file"

  echo "$file has been changed."
done

exit 0
