#!/bin/bash
# Daczo Alpar - daim1803 - 531
# A paraméterekként megadott html állományokat alakítsa egyszerű szöveges állománnyá
#(az állomány tartalmából töröljön mindent, ami < és > karakterek között van, beleértve azokat a karaktereket is).

if [ $# -lt 1 ]; then
  echo "Error: Please add atleast one filename as an argument!"
  echo "Usage: ./L05_11_daim1803.sh filename1 [filenames]"
  exit 1
fi

for file in "$@"; do
  # Ellenőrizzük, hogy az állomány létezik-e
  if [ ! -e "$file" ]; then
    echo "Warning: $file does not exist"
    continue
  fi

  # Ellenőrizzük, hogy az állomány valóban állomány-e
  if [ ! -f "$file" ]; then
    echo "Warning: $file is not a file"
    continue
  fi

  # Ellenőrizzük, hogy olvasható-e az állomány
  if [ ! -r "$file" ]; then
    echo "Warning: $file is not readable"
    continue
  fi

  # Módosítjuk az állomány tartalmát, törölve az < és > karakterek közötti részeket
  sed -i 's/<[^>]*>//g' "$file"

  echo "Az allomany $file modositva lett."
done

exit 0
