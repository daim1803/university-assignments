// Daczo Alpar - 531 - daim1803
#include "distance.h"
#include <math.h>

static double euclTav(double x, double y) {
    return (x-y) * (x-y);
}

double tavolsag(double *x, double *y, int n) {
    double sum = 0.0;
    int it = 0;

    for (; it < n; it++) {
        sum += euclTav(*x, *y);
	x++; y++;
    }

    return sqrt(sum);
}
