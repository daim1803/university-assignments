// Daczo Alpar - 531 - daim1803
// Irjunk egy tavolsag(x, y, n) függvényt, amelyik kiszámítja az x=(xi) és y=(yi), i=1, ..., n vektorok euklideszi távolságát (a vektorok azonos rangú elemei különbsége négyzetösszegének a négyzetgyöke). Ezt a függvényt használjuk 2 vektor távolságának kiszámítására. A vektorokat az input nevű állományból olvassuk be, az eredményt, a vektorok euklideszi távolságát, pedig az output nevű állományba írjuk.
#include <stdio.h>
#include <stdlib.h>
#include "distance.h"
#include <math.h>

int main() {
    FILE *fin;

    if ((fin=fopen("input", "r")) == NULL) {
        printf("Hiba: Nem sikerult megnyitni az input filet\n");
        return 1;
    }

    int size = 0;

    // A 2 vektor beolvasasa
    double array[10000];
    while (fscanf(fin,"%lf",&array[size]) != EOF) {
	size++;
    }

    // Tavolsag kiszamolasa
    double result = tavolsag(array, array + (size / 2), size / 2);

    // Valasz kiirasa
    FILE *fout = fopen("output", "w");
    fprintf(fout, "A ket vektor kozotti tavolsag: %lf\n", result);
    fclose(fout);
    return 0;
}
