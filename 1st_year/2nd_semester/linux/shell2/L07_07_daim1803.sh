#!/bin/bash
# Daczo ALpar - 531 - daim1803

# Írjunk felügyelő programot, amely a joe illetve vi(m) programok használatát figyeli a rendszerben. A várakozási időt paraméterként másodpercben adjuk meg (továbbiakban idő)
# idő intervallumonkét egy log nevű állományba lementi, hogy:
# hány különböző felhasználó futtat joe-t, illetve hányan használnak vi(m)-t
# amennyiben az előző megfigyelésnél valamelyik felhasználó használta a vi(m)-t, a jelenleginél viszont már nem futtatja, írjuk ki a képernyőre, hogy [felhasználónév] intelligens felhasználó :),
# és amennyiben az illető csoporttárs, küldjünk neki (poénból) egy elismerő üzenetet, amiért sikeresen kilépett a vi(m) programból.
# 12*idő intervallumonként írjuk ki képernyőre, hogy:
# az utóbbi 12*idő intervallumon, illetve
# a program indítása óta történt megfigyeléseket figyelembe véve mennyi és mikor volt a "rekord" a joe, illetve vi(m) szövegszerkesztőket használók számát tekintve.
# az utóbbi 12*idő intervallum megfigyeléseinek átlagát tekintve nőtt-e vagy csökkent a joe, illetve vi(m) programokat használók száma az előző megfigyelési ciklushoz képest.

# A log file torlese
rm -rf log


# Parameter ellenorzes
if [ ! $# -eq 1 ]; then
    echo "Error: The script requires one argument"
    echo "Usage: ./L07_07_daim1803.sh time"
    exit 1
fi

time=$1

if [ "$time" -lt 1 ]; then
    echo "Error: The time argumnet must be between 1 and 120"
    exit 1
fi

if [ "$time" -gt 120 ]; then
    echo "Error: The time argumnet must be between 1 and 120"
    exit 1
fi

my_group="gr531"
# A vi_usersben taroljuk azon felhasznaloneveket amelyek vim-et nyitottak meg egy ido*12-es ciklus folyaman
declare -A vi_users
declare -A joe_users
# Szamoljuk az iteraciokat egy ciklushoz
counter=1
cycle=1

nr_vi_users_in_cycle=0
nr_joe_users_in_cycle=0
active_vi_users=0
active_joe_users=0
max_vi_users=0
max_joe_users=0
max_vi_users_cycle=0
max_joe_users_cycle=0
max_vi_users_it=0
max_joe_users_it=0
avg_vi_users=0.0
avg_joe_users=0.0

# Kezdjuk a felugyelo programot
while true; do

    # A vi(m)-et hasznalo felhasznalok, lehetnek duplikatumok
    w | grep "vi" | cut -d" " -f1 > tmp_file
    input="./tmp_file"

    # A vi(m)-et hasznalok mindig kapnak egy timestampet, hogy melyik iteracioban hasznaltak utoljara a vim-et.
    # Ha valakinek a kovetkezo korben nem counter lesz a timestamp akkor bezarta a vim-et
    while IFS= read -r username; do
        if [ ! "${vi_users[$username]}" ]; then
            nr_vi_users_in_cycle=$((nr_vi_users_in_cycle + 1))
        fi
        # Az asszociativ tombbel kiszurjuk a felhasznalokat akik tobbszor is hasznaltak a vim-et az iteracio soran
        vi_users[${username}]=${counter}

    done < "$input"
    rm -rf tmp_file

    # Megnezzuk, hogy valaki zarta-e be a vi(m)-et, ha valaki bezarta, akkor az erteke nem megegyezo a counter ertekkel
    for username in "${!vi_users[@]}"
    do
        username=$(cat /etc/pseudopasswd | grep "$username" | cut -f1 -d:)
        # echo "debug: ${username} ${vi_users[$username]}"
        if [ ! "${vi_users[$username]}" -eq ${counter} ]; then
            if [ ! "${vi_users[$username]}" -eq -1 ]; then
                echo "${username} egy intelligens felhasznalo. :)" >> log
		if [[ $(groups "$username" | cut -d":" -f2 | cut -d" " -f2) = "${my_group}" ]]; then
			echo "Gratulalok, hogy sikerult bezarni a vi(m)-et!" | write "${username}" >> log
		fi
                # Aki kielepett annak -1re allitjuk a timestampjet, hogy tobbszor ne irjuk ki, hogy mar kilepett
                vi_users[$username]=-1
            fi
        else
            active_vi_users=$((active_vi_users + 1))
        fi
    done

    # joe-t hasznalo felhasznalok megszamolasa
    w | grep "joe" | cut -d" " -f1 > tmp_file

    while IFS= read -r username; do
        if [ ! "${joe_users[$username]}" ]; then
            active_joe_users=$((active_joe_users + 1))
            nr_joe_users_in_cycle=$((nr_joe_users_in_cycle + 1))
            joe_users[${username}]=1
        fi

    done < "$input"
    rm -rf tmp_file
    unset joe_users
    declare -A joe_users

    if [ "${active_vi_users}" -gt ${max_vi_users} ]; then
        max_vi_users=${active_vi_users}
        max_vi_users_it=${counter}
        max_vi_users_cycle=${cycle}
    fi

    if [ "${active_joe_users}" -gt ${max_joe_users} ]; then
        max_joe_users=${active_joe_users}
        max_joe_users_it=${counter}
        max_joe_users_cycle=${cycle}
    fi

    counter=$((counter + 1))

    # Veget er egy ciklus
    if [ "${counter}" -eq 12 ]; then

        #Kiirjuk a ciklusra vonatkozo informaciokat
        echo "A legtobben a ${max_vi_users_cycle}. ciklus ${max_vi_users_it}. iteraciojaban hasznaltak a vi(m) szovegszerkesztot, szamszerint ${max_vi_users} szemely."
        echo "A legtobben a ${max_joe_users_cycle}. ciklus ${max_joe_users_it}. iteraciobjaban hasznaltak a joe szovegszerkesztot, szamszerint ${max_joe_users} szemely."

        # Atlag vi users a ciklusban
        # Kiszamoljuk az adott ciklusban vi(m)et hasznalo atlag felhasznalo szamot
        # A bash nem tamogatja a floating point szamokkal valo dolgozas ezert awk-t hasznalok.
        echo "${nr_vi_users_in_cycle}" >> tmp_file
        this_cycle_vi_avg=$(awk -v num="${avg_vi_users}" '{
            printf("%f\n", $1 / 12);
        }' tmp_file
        rm -rf tmp_file)

        # Osszehasonlitjuk az elozo ciklusbeli atlag felhasznalo szamot a mostanival
        echo "${this_cycle_vi_avg} ${avg_vi_users}" >> tmp_file
        awk '{
            if($1 > $2) {
                printf("A vi(m)-et hasznalo felhasznalok atlag szama nott az elozo ciklushoz hasonlitva\n");
            }
            else if ($1 < $2) {
                printf("A vi(m)-et hasznalo felhasznalok atlag szama csokkent az elozo ciklushoz hasonlitva\n");
            }
            else {
                printf("A vi(m)-et hasznalo felhasznalok atlag szama nem valtozott az elozo ciklushoz hasonlitva\n");
            }
        }' tmp_file
        rm -rf tmp_file
        # A mostani atlag felhasznalo szamot elmentjuk, hogy a kovetkezo ciklus vegen is felhasznaljuk
        avg_vi_users=${this_cycle_vi_avg}


        # Atlag joe users a ciklusban
        # Kiszamoljuk az adott ciklusban joe-t hasznalo atlag felhasznalo szamot
        # A bash nem tamogatja a floating point szamokkal valo dolgozas ezert awk-t hasznalok.
        echo "${nr_joe_users_in_cycle}" >> tmp_file
        this_cycle_joe_avg=$(awk '{
            printf("%f\n", $1 / 12);
        }' tmp_file
        rm -rf tmp_file)

        # Osszehasonlitjuk az elozo ciklusbeli atlag felhasznalo szamot a mostanival
        echo "${this_cycle_joe_avg} ${avg_joe_users}" >> tmp_file
        awk '{
            if($1 > $2) {
                printf("A joe-t hasznalo felhasznalok atlag szama nott az elozo ciklushoz hasonlitva\n");
            }
            else if ($1 < $2) {
                printf("A joe-t hasznalo felhasznalok atlag szama csokkent az elozo ciklushoz hasonlitva\n");
            }
            else {
                printf("A joe-t hasznalo felhasznalok atlag szama nem valtozott az elozo ciklushoz hasonlitva\n");
            }
        }' tmp_file
        rm -rf tmp_file
        # A mostani atlag felhasznalo szamot elmentjuk, hogy a kovetkezo ciklus vegen is felhasznaljuk
        avg_joe_users=${this_cycle_joe_avg}

        unset vi_users
        declare -A vi_users
        counter=1
        nr_vi_users_in_cycle=0
        nr_joe_users_in_cycle=0
        cycle=$((cycle + 1))
        # Ures sor a jobb olvashatosagert
        echo ""
    fi

    
    {
        echo "Aktiv felhasznalok szama, akik vi(m)-t hasznalnak: ${active_vi_users}";
        echo "Aktiv felhasznalok szama, akik joe-t hasznalnak: ${active_joe_users}";
        echo ""
    } >> log

    active_vi_users=0
    active_joe_users=0
    sleep "$time"
done
