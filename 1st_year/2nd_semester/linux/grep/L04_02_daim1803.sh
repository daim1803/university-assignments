#!/bin/bash
# Daczo Alpar - daim1803 - 531
# Írassa ki a paraméterként megadott szöveges állomány(ok) közül azoknak a nevét és számát,
# amelyek egy ugyancsak paraméterként megadott szót tartalmaznak. A paraméterek sorrendje: szó állományn(év/evek).

# Ellenőrizzük, hogy pontosan két paramétert adtak-e meg
if [ $# -lt 2 ]; then
  echo "Error! Usage: ./nev.sh szo allomany1 [allomany2..n]"
  exit 1
fi

szo=$1

# Ellenőrizzük, hogy a szó nem üres
if [ -z "$szo" ]; then
  echo "Error: The word must be a non empty string"
  exit 1
fi

counter=0
declare -A files
shift
for text_file in "$@"; do
    # Ellelorizzuk, hogy az allomany valoban allomany
    if [ -e "$text_file" ]; then
        # Ellenorizzuk, hogy az allomany valoban allomany
        if [ -f "$text_file" ]; then
            # Ellenorizzuk, hogy az allomany olvashato-e
            if [ -r "$text_file" ]; then
                # Ellenorizzuk, hogy a szo megtalalhato-e az allomanyban
                if [ ! "$(grep -c "$szo" "$text_file")" -eq 0 ]; then
                    files["$counter"]="$text_file"
                    counter=$((counter + 1))
                fi
            else
                echo "Warning: ${text_file} is not readable"
            fi
        else
            echo "Warning: ${text_file} is not a file"
        fi
    else
        echo "Warning: $text_file does not exist"
    fi
done

echo "Number of files containing the $szo word: $counter"
for key in "${!files[@]}"; do
    echo "${files[$key]}"
done

exit 0
