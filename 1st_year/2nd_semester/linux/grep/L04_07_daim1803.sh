#!/bin/bash
# Daczo Alpar - daim1803 - 531
# Írassa ki a paraméterként megadott szerver(ek)re, hogy elérhető-e, vagy sem.
# Tipp! Annak eldöntésére, hogy egy szerver elérhető-e vagy sem a ping parancsot használhatjuk.
# Az eredményt a következő formában írjuk ki:
# [hostname/IP cim] - [elerheto/nem elerheto]
# ahol a [hostname/IP cim] helyett a megfelelő szerver nevét, [elerheto/nem elerheto] helyére pedig az elérésére vonatkozó megfelelő információt írjuk.


# Ellenőrizzük, hogy legalább egy paramétert megadtak-e
if [ $# -eq 0 ]; then
  echo "Hiba: legalabb egy szervernevet vagy IP címet meg kell adni paraméterkent!"
  exit 1
fi

for server in "$@"; do
  result=$(ping -q -c3 "$server" 2> tmp)
  ret_code="$?"
  if [ "$ret_code" -eq 2 ]; then
    echo "$server nem letezo szerver nem vagy ip cim"
  elif [ "$ret_code" -eq 1 ]; then
    echo "$server nem elerheto"
  elif [ "$(grep -c " 0% packet loss")" "$result" -eq 1 ]; then
    echo "$server elerheto"
  fi
done

rm -rf tmp
