// // Daczo Alpar - diam1803 - 531

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

// Method to write array into a file with a given file descriptor
// @param size - size of the array
// @param arr - array to display
// @param f - file descriptor
void displayArr(int size, int arr[], FILE *f) {
    int index;
    for (index = 0; index < size; index++) {
        fprintf(f, "%d ", arr[index]);
    }
    fprintf(f, "\n");
}

// Method to merge 2 arrays into another array
// @param arr - the merged array
// left - first array to merge
// left_size - first array size
// right - second array to merge
// right_size - second array size
void merge(int arr[], int left[], int left_size, int right[], int right_size) {
    int i = 0, j = 0, k = 0;

    while (i < left_size && j < right_size) {
        if (left[i] <= right[j]) {
            arr[k++] = left[i++];
        } else {
            arr[k++] = right[j++];
        }
    }

    while (i < left_size) {
        arr[k++] = left[i++];
    }

    while (j < right_size) {
        arr[k++] = right[j++];
    }
}

void merge_sort(int arr[], int size, FILE *flog) {
    if (size <= 1) {
        return;
    }

    int middle = size / 2;

    // pipes
    int pipefd[2];
    int pipeToChild1[2];

    if (pipe(pipefd) == -1) {
        perror("Pipe error");
        exit(1);
    }

    if (pipe(pipeToChild1) == -1) {
        perror("Pipe error");
        exit(1);
    }

    // Creating the 1st child
    pid_t child1_pid = fork();
    if (child1_pid == -1) {
        perror("Fork error");
        exit(1);
    }

    if (child1_pid == 0) {
        // First child proccess
        // Closing not used pipes
        close(pipeToChild1[1]);
        close(pipefd[0]); // Only writing is needed

        // Reading the left side of the array from the parent
        int left[middle];
        read(pipeToChild1[0], left, middle * sizeof(int));
        close(pipeToChild1[0]);
        
        merge_sort(left, middle, flog);

        fprintf(flog, "gyerek - %d - 1. resz sorozat: ", getpid());
        displayArr(middle, left, flog);

        write(pipefd[1], left, middle * sizeof(int));
        close(pipefd[1]);

        exit(EXIT_SUCCESS);
    } else {
        // Parent
        // Closing not used pipe
        close(pipeToChild1[0]);
        
        // Writing the left side of the array to the first child
        write(pipeToChild1[1], arr, middle * sizeof(int));
        close(pipeToChild1[1]);
        
        int pipeToChild2[2];

        if (pipe(pipeToChild2) == -1) {
        perror("Pipe error");
        exit(1);
        }
        
        // Creating the second child
        pid_t child2_pid = fork();
        if (child2_pid == -1) {
            perror("Fork error");
            exit(1);
        }

        if (child2_pid == 0) {
            // Second child
            // Closing not used pipes
            close(pipeToChild2[1]);
            close(pipefd[0]); // Only writing is needed
            
            // Reading the right side of the array from parent
            int right[size - middle];
            read(pipeToChild2[0], right, (size - middle) * sizeof(int));

            merge_sort(right, size - middle, flog);

            fprintf(flog, "gyerek - %d - 2. resz sorozat: ", getpid());
            displayArr(size - middle, right, flog);

            write(pipefd[1], right, (size - middle) * sizeof(int));
            close(pipefd[1]);

            exit(EXIT_SUCCESS);
        } else {
            // Parent
            // Closing not needed pipes
            close(pipeToChild2[0]);
            close(pipefd[1]); // Only reading is needed

            // Sending child 2 the right side of the array    
            write(pipeToChild2[1], arr + middle, (size - middle) * sizeof(int));
            close(pipeToChild2[1]);

            // Reading the sorted array from child1
            int child1_arr[middle];
            read(pipefd[0], child1_arr, middle * sizeof(int));

            // Reading the sorted array from child2
            int child2_arr[size - middle];
            read(pipefd[0], child2_arr, (size - middle) * sizeof(int));
            close(pipefd[0]);

            // Waiting child processes to end
            wait(NULL);
            wait(NULL); 

            merge(arr, child1_arr, middle, child2_arr, size - middle);
            
            fprintf(flog, "szulo - %d - rendezett sorozat: ", getpid());
            displayArr(size, arr, flog);
        }
    }
}

int main() {
    FILE *fin = fopen("input","r");
    if (fin == NULL) {
        perror("fopen error");
        exit(1);
    }
    
    FILE *flog = fopen("log", "w");
    int size;

    // Read the number of elelments in the array
    int pfd[2]; // pipe
    fscanf(fin, "%d", &size);

    // Allocate the array and read the input elements
    int *arr = (int*)malloc(size * sizeof(int));
    int index = 0;
    for (; index < size; index++) {
        fscanf(fin, "%d", &arr[index]);
    }

    merge_sort(arr, size, flog);

    // Writing out the result
    FILE *fout = fopen("output","w");
    displayArr(size, arr, fout);
    printf("\n");

    return 0;
}
