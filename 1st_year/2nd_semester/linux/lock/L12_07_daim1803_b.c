#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

int main()
{
    FILE *fin = fopen("matrix","r");

    if (fin == NULL) 
    {
        perror("Error while opening the matrix file!");
        exit(1);
    }

    // Reading the matrix out of the file
    int n;
    int arr[MAX_BUFFER_SIZE];
    int i;
    fscanf(fin, "%d", &n);
    
    for (i = 0; i < n*n; i++)
    {
        fscanf(fin, "%d", &arr[i]);
    }

    int fd = open("matrix.bin", O_WRONLY | O_CREAT,  0644);

    if (fd == -1)
    {
        perror("Error while opening the matrix.bin file!");
        exit(1);
    }

    // Writing the matrix in the binary file
    write(fd, &n, sizeof(int));
    write(fd, arr, n * n * sizeof(int));

    fclose(fin);
    close(fd);
    return 0;
}