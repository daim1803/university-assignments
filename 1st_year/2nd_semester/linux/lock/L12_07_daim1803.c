#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

void dispMatrix(int *a, int n)
{
    for (int i = 0; i < n*n; i++) {
        printf("%d ", a[i]);
        if ((i+1) % n == 0) printf("\n");
    }
}

int random_num(int min, int max) 
{
    return (rand() % (max - min + 1)) + min;
}

int getPositionInVector(int n, int row, int column)
{
    return row * n + column;
}

int getNumbersInMatrix(int n, int *arr)
{
    int i = 0;
    int counter = 0;
    for (i = 0; i < n; i++) 
    {
        if (arr[i] != 0) counter++;
    }

    return counter;
}

int main() {
    srandom(getpid());

    int code;
    int fd_matrix = open("matrix.bin", O_RDWR);
    if (fd_matrix == -1)
    {
        perror("Error while opening the matrix.bin file!");
        exit(1);
    }

    struct flock lockStruct;
    memset(&lockStruct, 0, sizeof(struct flock));

    // set the lock
    lockStruct.l_type = F_WRLCK;
    lockStruct.l_whence = SEEK_SET;
    lockStruct.l_len = 8;

    FILE *fd_log = fopen("log","a");

    int n;
    read(fd_matrix, &n, sizeof(int));
    int it;
    // column or row sum
    // int sum_type = random_num(1,2);
    // int constant = random_num(0, n - 1);
    for (it = 0; it < MAX_ITERATION; it++) 
    {
        int sum_type = random_num(1,2);
        int constant = random_num(0, n - 1);
        // skip the first integer of the matrix.bin file
        lseek(fd_matrix, 4, SEEK_SET);
        int arr[MAX_BUFFER_SIZE];
        read(fd_matrix, &arr, n * n * sizeof(int));

        int i;
        for (i = 0; i < n - 1; i++) 
        {
            lseek(fd_matrix, 4, SEEK_SET);
            int pos;
            switch (sum_type)
            {
            case 1:
                // row sum
                pos = getPositionInVector(n, constant, i);
                if (arr[pos]) {
                    fprintf(fd_log, "writer %d: Blokalni probalok.\n",getpid());
                    code = fcntl(fd_matrix, F_SETLKW, &lockStruct);

                    if (code < 0) {
                        perror("fcntl error!");
                        exit(1);
                    }

                    fprintf(fd_log, "writer %d: Sikeres blokalas.\n",getpid());
                    arr[pos + 1] += arr[pos];
                    arr[pos] = 0;

                    // change it in the matrix.bin
                    lseek(fd_matrix, pos, SEEK_CUR);
                    write(fd_matrix, arr + pos, 2 * sizeof(int));

                    lockStruct.l_type = F_UNLCK;

                    code = fcntl(fd_matrix, F_SETLKW, &lockStruct);
                    if (code < 0) {
                        fprintf(fd_log, "writer %d: Hiba a zar feloldasanal!\n",getpid());
                        exit(1);
                    }
                    fprintf(fd_log, "writer %d: Sikeres zar feloldas!\n",getpid());
                }
                break;
            default:
                pos = getPositionInVector(n, i, constant);
                if (arr[pos]) {
                    fprintf(fd_log, "writer %d: Blokalni probalok.\n",getpid());
                    code = fcntl(fd_matrix, F_SETLKW, &lockStruct);
                    lockStruct.l_len = 4 * n;
                    if (code < 0) {
                        perror("fcntl error!");
                        exit(1);
                    }

                    fprintf(fd_log, "writer %d: Blokalni probalok.\n",getpid());
                    int nextPos = getPositionInVector(n, i + 1, constant);
                    arr[nextPos] += arr[pos];
                    arr[pos] = 0;

                    // change it in the matrix.bin
                    lseek(fd_matrix, pos, SEEK_CUR);
                    write(fd_matrix, arr + pos, sizeof(int));
                    lseek(fd_matrix, 4, SEEK_SET);
                    lseek(fd_matrix, nextPos, SEEK_CUR);
                    write(fd_matrix, arr + nextPos, sizeof(int));

                    lockStruct.l_type = F_UNLCK;

                    code = fcntl(fd_matrix, F_SETLKW, &lockStruct);
                    if (code < 0) {
                        fprintf(fd_log, "writer %d: Hiba a zar feloldasanal!\n",getpid());
                        exit(1);
                    }
                    fprintf(fd_log, "writer %d: Sikeres zar feloldas!\n",getpid());
                }
                break;
            }
        }

        if (getNumbersInMatrix(n, arr) == 1)
        {
            FILE *fout = fopen("output", "w");
            fprintf(fout, "%d\n", arr[n-1]);
            fclose(fout);
            break;
        }

    }

    close(fd_matrix);
    close(fd_matrix);
    fclose(fd_log);
    return 0;
}