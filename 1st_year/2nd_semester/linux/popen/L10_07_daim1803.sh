#!/bin/bash
# Daczo Alpar - 531 - daim1803

if [ $# -lt 1 ]; then
  echo "Error: A directory name is expected as an argument"
  echo "Usage: ./L10_07_daim1803.sh directory_name"
  exit 1
fi

directory=$1

if [ ! -e "${directory}" ]; then
    echo "Error: ${directory} does not exist"
    exit 1
fi

  # Ellenőrizzük, hogy az állomány valóban állomány-e
if [ ! -d "${directory}" ]; then
    echo "Error: ${directory} is not a directory"
    exit 1
fi

# Ellenőrizzük, hogy olvasható-e az állomány
if [ ! -r "${directory}" ]; then
    echo "Error: ${directory} is not readable"
    exit 1
fi

find $directory -type f -name '*.c'
