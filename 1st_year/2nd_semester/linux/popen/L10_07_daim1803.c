// Daczo Alpar - 531 - daim1803
// A kliens egy katalógusnevet küld a szervernek, amelyet az input nevű állományból olvas. 
// A szerver pedig visszaküldi az illető katalógusban és ennek alkatalógusaiban szereplő összes C forráskódot tartalmazó állomány nevét.

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

#define MAX_FILE_NAME 256

int main()
{
    FILE *fin = fopen("input", "r");
    if (fin == NULL)
    {
        perror("fopen");
        return 1;
    }

    char catalog_name[MAX_FILE_NAME];
    fgets(catalog_name, sizeof(catalog_name), fin);
    catalog_name[strcspn(catalog_name, "\n")] = '\0';

    char command[2 * MAX_FILE_NAME + 14];
    sprintf(command, "./L10_07_daim1803.sh %s", catalog_name);

    FILE *pipe = popen(command, "r");

    if (pipe == NULL)
    {
        perror("popen");
        return 1;
    }

    char file_name[MAX_FILE_NAME];
    while (fgets(file_name, sizeof(file_name), pipe) != NULL)
    {
        file_name[strcspn(file_name, "\n")] = '\0'; 
        printf("%s\n", file_name);
    }

    pclose(pipe);
    fclose(fin);

    return 0;
}
