# Daczó Alpár - daim1803 - 531
# Írjon shell script-et, amely kiírja a paraméterként megadott felhasználók folyamatainak a számát [user]:[count] formában, ahol a [user] helyére a felhasználónév, míg a [count] helyére a folyamatainak a száma kerüljön.
# Végezetül írjuk ki, hogy melyik felhasználó (vagy felhasználók) futtatják a legtöbb folyamatot, és ezen felhasználó(k) esetén írjuk is ki a futtatott folyamatokat úgy, hogy (egy adott felhasználó esetén) mindegyik folyamat csak egyszer szerepeljen, a következő formában:
# [user]
# [process name 1]
# [process name 2]
# ...
# ahol a [user] helyére a megfelelő felhasználónév, míg a [process name ...] helyére a folyamat neve kerüljön.
# Megyjegyzés! Amennyiben a paraméterlistában nemlétező felhasználónévvel találkozunk, írjuk ki a
#[user] nem letezik
# üzenetet, ahol a [user] helyére a megfelelő felhasználónév kerüljön, majd folytassuk a feladat végrehajtását a többi felhasználónév feldolgozásával.

#!/bin/bash

# létrehozunk egy asszociatív tömböt, hogy számoljuk a folyamatokat felhasználónként
if [ $# -eq 0 ]; then 
  echo "Usage: ./L03_07_daim1803.sh username1 username2 ... usernameN"
  exit 1
fi 

declare -A process_count

# végigmegyünk a paraméterlistán és kiszámoljuk a folyamatokat
for user in "$@"; do
  count=0
  if [ "$(getent passwd "$user")" ]; then
    count=$(ps -u "$user" | wc -l)
  fi
  if [ "$count" -eq -0 ]; then
    echo "$user nem létezik"
  else
    process_count["$user"]=$((count-1))
    echo "$user:${process_count[$user]}"
  fi
done

# meghatározzuk a legtöbb folyamattal rendelkező felhasználót
max_count=0
for user in "${!process_count[@]}"; do
  if [ ${process_count[$user]} -gt $max_count ]; then
    max_count=${process_count[$user]}
  fi
done

# kiírjuk a legtöbb folyamattal rendelkező felhasználó(k) nevét
echo -n "A legtöbb folyamattal rendelkező felhasználó(k): "
for user in "${!process_count[@]}"; do
  if [ ${process_count[$user]} -eq $max_count ]; then
    echo -n "$user "
  fi
done
echo ""

# kiírjuk a legtöbb folyamattal rendelkező felhasználó(k) futtatta folyamatokat
for user in "${!process_count[@]}"; do
  if [ ${process_count[$user]} -eq $max_count ]; then
    echo "$user"
    ps -u "$user" | rev | cut -d ' ' -f 1 | rev
  fi
done
exit 0
