# Daczó Alpár - daim1803 - 531
import requests
import string
import nltk
from nltk import FreqDist
from math import log2
# It's necessary to download this package
nltk.download('punkt')

def prepare_text(text: str):
    text = text.lower()
    text = text.translate(str.maketrans('', '', string.punctuation))
    text = text.replace("\n", " ").replace("\r", " ")
    text = " ".join(text.split())
    return text

def calc_entropy(freq_dist: FreqDist, words: list):
    entropy = 0
    for word in freq_dist:
        prob = freq_dist[word] / len(words)
        entropy -= prob * log2(prob)
    return entropy

def calc_lang_entropies(lang: str, url: str):
    print('Downloading the content of the book...')
    text = requests.get(url).text
    text = prepare_text(text)
    tokens = nltk.word_tokenize(text)
    words = [word for word in tokens if word.isalpha()]

    bigrams = list(nltk.bigrams(words))
    trigrams = list(nltk.trigrams(words))

    freq_dist = FreqDist(words)
    bigrams_freq_dist = FreqDist(bigrams)
    trigrams_freq_dist = FreqDist(trigrams)

    # Calculating entropy
    monograms_entropy = calc_entropy(freq_dist, words)
    bigrams_entropy = calc_entropy(bigrams_freq_dist, bigrams)
    trigrams_entropy = calc_entropy(trigrams_freq_dist, trigrams)

    print(f'The chosen language: {lang}')
    print(f'Entropy of monograms: {monograms_entropy}')
    print(f'Entropy of bigrams: {bigrams_entropy}')
    print(f'Entropy of trigrams: {trigrams_entropy}\n')

if __name__ == '__main__':
    url_hu = 'https://mek.oszk.hu/04700/04772/04772.htm'
    url_en = 'https://www.gutenberg.org/cache/epub/2701/pg2701-images.html'

    calc_lang_entropies('Hungarian', url_hu)
    calc_lang_entropies('English', url_en)