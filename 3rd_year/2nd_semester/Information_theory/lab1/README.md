# Language entropy

    The following script will calculate the entropy of the hungarian and english language based on books from the internet.

    It will calculate the enrtopy for:
    a.) monograms
    b.) bigrams
    c.) trigrams

## What is enrtropy?

    In information theory, the entropy of a random variable is the average level of "information", "surprise", or "uncertainty" inherent to the variable's possible outcomes.

    For the languages I calculated the enrtopy with the following formula:

        E = ∑( -probability_of_a_word * log2(probability_of_a_word))


## Packages you might need

    There are some packages you might need to download with pip before running the script.

    These packages are the following:

        1. requests
        2. nltk

## How to run the script

Run the following command:

```
py language_entropy.py
```