# Daczo Alpar - 531 - daim1803

def hamming_distance(string1: str, string2: str) -> int:
    """Return the Hamming distance between two strings."""
    if len(string1) != len(string2):
        raise Exception("Strings must be of equal length.")
    dist_counter = 0
    for n in range(len(string1)):
        if string1[n] != string2[n]:
            dist_counter += 1
    return dist_counter

if __name__ == '__main__':
    # Specify the input file name
    filename = 'input'
    with open(filename, 'r') as file:
        code = file.read().splitlines()

    dmin = 100
    for i in range(0, len(code) - 1):
        for j in range(1, len(code)):
            if i != j:
                dist = hamming_distance(code[i],code[j])
                if dist < dmin:
                    dmin = dist

    print(f'Errors detected: {dmin - 1}')
    print(f'Errors corrected: {int((dmin - 1) / 2)}')
