# Error correction

    Given a binary error correction code in an input file, each line contains a code word.
    The script implements a solution to display how much error can be detected and corrected.

## Theory behind the solution

    For the solution the minimum Hamming distance must be calculated.
    A code with minimum Hamming distance d between its codewords can detect at most d-1 errors and can correct ⌊(d-1)/2⌋ errors.

## How to run the script

```
py error_correction.py input_file
```

