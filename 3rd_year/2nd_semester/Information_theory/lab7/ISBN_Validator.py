# Daczo Alpar - daim1803 - 531

from sys import argv

def check_ISBN10(numbers: list) -> int:
    '''
    Function to calculate the check number of an ISBN10 code.
    The code given as argument can contain spaces and '-'
    IN: a list of integers
    OUT: int
    '''

    # Calculating the sum of number * weight, descending from 10 to 1
    sum = 0
    for index in range(0, len(numbers) - 1):
        sum += numbers[index] * (10 - index)

    # We need the remainder of the sum divided by 11
    remainder = sum % 11

    # Calculating the check number with 11 - remainder
    return 11 - remainder

def check_ISBN13(numbers: list) -> int:
    '''
    Function to calculate the check number of an ISBN13 code.
    The code given as argument can contain spaces and '-'
    IN: a list of integers
    OUT: int
    '''

    # Calculating the sum of numbers, from left to right, starting with 1, multipling every number on even position
    sum = 0
    for index in range(0, len(numbers) - 1):
        sum += numbers[index] if index % 2 == 0 else 3 * numbers[index]

    # Calculating the check number with 10 - (Sum mod 10)
    return 10 - (sum % 10)

def validate_ISBN(ISBN_num: str) -> None:
    '''
    Method to validate an ISBN10 or ISBN13 code
    The code given as argument can contain spaces and '-' characters
    If the code is valid the function will display: "Helyes!"
    Else it will display: "Helytelen. Helyesen: " and the correct check number
    '''
    # Storing the actual numbers from the code in a list
    numbers = []

    for char in ISBN_num:
        if char >= '0' and char <= '9':
            numbers.append(int(char))

    length = len(numbers)
    if length != 10 and length != 13:
        print('Error: Invalid ISBN10 code! The code must contain strictly 10 or 13 numbers!')

    correct_check_num = check_ISBN10(numbers) if length == 10 else check_ISBN13(numbers)

    if correct_check_num == numbers[-1]:
        print('Helyes!')
    else:
        print(f'Helytelen. Helyesen: ', end='')
        print('X') if correct_check_num == 10 else print(correct_check_num)
    print()

if __name__ == '__main__':
    '''
    ISBN validator script

    The script requires an input file given as an argument.
    The file must have an ISBN code in each line
    The codes can contain numbers and ' ', '-' characters

    The script will display the code and if it is a valid ISBN code or not.
    If the code is invalid it will also display what would be its correct check number.
    '''
    if len(argv) != 2:
        print('Error: The script must have a filename as an argument!')
        print('Usage: py ISBN_Validator.py input_file_name')
        exit(1)

    input_file_name = argv[1]

    try:
        with open(input_file_name) as input_file:
            rows = input_file.readlines()

            for ISBN_code in rows:
                ISBN_code = ISBN_code.rstrip()

                print(ISBN_code)
                validate_ISBN(ISBN_code)
    except FileNotFoundError:
        print(f"Error: {input_file_name} file does not exist!")
    except IOError:
        print('Error: An error has occoured while reading the file!')
    except Exception as err:
        print(f'{err}')
