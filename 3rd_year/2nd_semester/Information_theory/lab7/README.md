 # ISBN validator script

    The script requires an input file given as an argument.
    The file must have an ISBN code in each line
    The codes can contain numbers and ' ', '-' characters

    The script will display the code and if it is a valid ISBN code or not.
    If the code is invalid it will also display what would be its correct check number.

## How to run?

You have to run the following command:

```
py ISBN_Validator.py input_file
```

I provided an example input file, you can use the input.txt as the input file