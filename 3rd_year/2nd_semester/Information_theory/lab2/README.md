# Entropies

    This script will calculate different type of entropies given 2 or more binary (Bernoulli) probability variables in an input file.

# How to run?

```
py entropies.py input_file
```