# Daczo Alpar - daim1803 - 531
import math
import sys




if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Error: Not enough arguments given')
        print('Usage: py entropies.py input_file')
        exit(1)

    input_file = sys.argv[1]

    with open(input_file, "r") as f:
        p_x = float(f.readline())
        p_y_0 = float(f.readline())
        p_y_1 = float(f.readline())

        q_x = 1 - p_x
        q_y_0 = 1 - p_y_0
        q_y_1 = 1 - p_y_1

        # H(X) - X entropy
        h_x = - (p_x * math.log2(p_x) + q_x * math.log2(q_x))

        # H(Y) - Y entropy
        p_y = p_x * p_y_1 + q_x * p_y_0
        q_y = 1 - p_y
        h_y = - (p_y * math.log2(p_y) + q_y * math.log2(q_y))

        # H(X,Y) - joint entropy
        hxy = - (q_x * q_y_0 * math.log2(q_x * q_y_0)\
                + q_x * p_y_0 * math.log2(q_x * p_y_0)\
                + p_x * q_y_1 * math.log2(p_x * q_y_1)\
                + p_x * p_y_1 * math.log2(p_x * p_y_1))

        # Conditional entropy
        # H(X|Y)
        h_xy = hxy - h_y

        # H(Y|X)
        h_yx = hxy - h_x

        # I(X;Y) - mutual information
        i_xy = h_x - h_xy

        # Kullback–Leibler divergence - relative entropy
        # D(P(X)||P(Y))
        d_xy = p_x * math.log2(p_x / p_y) + q_x * math.log2(q_x / q_y)

        # D(P(Y)||P(X))
        d_yx = p_y * math.log2(p_y / p_x) + q_y * math.log2(q_y / q_x)

        # Displaying the results
        print(f'H(X) = {h_x:.6f}')
        print(f'H(Y) = {h_y:.6f}')
        print(f'H(X,Y) = {hxy:.6f}')
        print(f'H(X|Y) = {h_xy:.6f}')
        print(f'H(Y|X) = {h_yx:.6f}')
        print(f'I(X;Y) = {i_xy:.6f}')
        print(f'D(P(X)||P(Y)) = {d_xy:.6f}')
        print(f'D(P(Y)||P(X)) = {d_yx:.6f}')
