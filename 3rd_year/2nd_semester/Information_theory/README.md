# Information theory assignment

    In this folder you can find the assignments I submitted for the information theory subject

    List of assignments submitted:

        1. Entropy of a language
        2. Calculating different type of entropies
        6. Error detection and error correction
        7. ISBN validator

## Tools

    For running the assignments you will need python3.
    The assignments contains more detailed instructions about how to run them.
