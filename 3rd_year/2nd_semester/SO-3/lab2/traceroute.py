# Daczo Alpar - daim1803 - 531
import argparse
import socket
from scapy.all import *

def parse_arguments():
    parser = argparse.ArgumentParser(prog='Traceroute',
                                    description='Implementing a traceroute application in Python')
    parser.add_argument('target_name', type=str,
                        help='Target hostname or ip address')
    parser.add_argument('-m', type=int, default=30,
                        help='Maximum numer of jumps while we are looking for the target')
    parser.add_argument('-w', type=int, default=5,
                        help='Wait timeout milliseconds for each reply.')
    return parser.parse_args()

def traceroute(target: str, max_hops: int, timeout: int):
    ttl = 1
    while True:
        # create an ICMP echo request packet with the given TTL
        packet = IP(dst=target, ttl=ttl) / ICMP()

        # send the packet and record the time
        start_time = time.time()
        reply = sr1(packet, timeout=timeout, verbose=0)
        # calculate the elapsed time
        elapsed_time = (time.time() - start_time) * 1000
        host_name = socket.getfqdn(reply.src) if socket.getfqdn(reply.src) != reply.src else ""
        if not reply:
            # no reply received, hop timed out
            print(f'{ttl}. Hop timed out')
        elif reply.type == 0:
            # reply received, target reached
            print(f'{ttl}. {elapsed_time}ms {reply.src} ({host_name})')
            print(f'Reached the destination at {reply.src}')
            return
        else:
            # reply received, hop found
            print(f'{ttl}. {elapsed_time}ms {reply.src} ({host_name})')
        ttl += 1

        if ttl > max_hops:
            # maximum number of hops reached
            print(f'Destination not reached in {max_hops} hops!')
            break

if __name__ == '__main__':
    parser = parse_arguments()
    traceroute(parser.target_name, parser.m, parser.w)
