# Traceroute

The following script will implement a traceroute application.

You can trace a target given by an IP address or host name with sending IPCM request packets to the target and any address between us and the target.

## How to run

Use the following command:

```
py traceroute.py target [-m max_num_of_jumps] [- wait_timeout] [-h]
```