# Scrapy

The following assignment will gather data about the hungarian professors of the Babes-Bolyai University.

With using scrapy we gather the information from the webpage http://www.cs.ubbcluj.ro/magunkrol/a-kar-felepitese/magyar-matematika-es-informatika-intezet/, implementing a spider to gather information in a .csv file about the professors with the following fields:

1. name
2. email
3. webpage - the lecturer's webpage, empty if the the lecturer doesn't have one
4. page_ok - boolean, shows if we can connect to the webpage or not, empty if the lecturer doesn't have a webpage
5. cv - link to the lecturer's cv, empty if they don't have one
6. address
7. research
8. image_urls, file_urls,, images, files

It will also gather the cv files to the files folder and images about the lecturers in the images folder.

## How to run

Run the following command in the current folder:

```
    scrapy crawl tanarok -O teachers.csv
```

You can check the teachers.csv, with the vs code extension 'CSV to Table' installed you can format the file by selecting the rows, **excluding the last one** then in the command palette write 'Convert to table from CSV'