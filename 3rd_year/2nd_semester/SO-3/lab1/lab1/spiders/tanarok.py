# Daczó Alpár - daim1803 - 531
import scrapy

class TanarokSpider(scrapy.Spider):
    name = "tanarok"
    allowed_domains = ["http://www.cs.ubbcluj.ro/magunkrol/a-kar-felepitese/magyar-matematika-es-informatika-intezet/"]
    start_urls = ["http://www.cs.ubbcluj.ro/magunkrol/a-kar-felepitese/magyar-matematika-es-informatika-intezet/"]

    def process_data(self, data_arr, person):
        web_url = ''
        cv_url = ''
        address = ''
        research = ''

        for data in data_arr:
            urls = person.xpath('a/@href').getall()
            urls = urls.split(', ') if ',' in urls else urls

            for url in urls:
                if 'CV' in url:
                    cv_url = url
                else:
                    web_url = url
            if 'Cím' in data:
                address = data[len('Cím: '):]
            elif 'Szakterület' in data:
                research = data[len('Szakterület: ') + 1:]

        return web_url, cv_url, address, research

    def parse_httpbin(self, resp, prof):
        self.logger.info('Parse_httpbin meghivva')

        if resp.status == 200:
            prof['page_ok'] = 'true'
        else:
            prof['page_ok'] = 'false'

        yield prof

    def errback_httpbin(self, failure):
        self.logger.error('errback_httpbin meghivva')
        prof = failure.request.cb_kwargs['prof']
        prof['page_ok'] = 'false'
        yield prof

    def get_urls(self, raw_urls, resp):
        clean_urls = []

        for url in raw_urls:
            clean_urls.append(resp.urljoin(url))
        return clean_urls

    def parse(self, response):
        selector = response.xpath('//div[@class="entry clearfix"]/div/div')
        raw_img_urls = selector.xpath('img/@src').getall()
        raw_cv_urls = selector.xpath('a[contains(@href, ".pdf")]/@href').getall()

        clean_cv_urls = self.get_urls(raw_cv_urls, response)
        clean_img_urls = self.get_urls(raw_img_urls, response)

        for div in selector:
            data = div.xpath('text()').getall() # Data about a professor

            if data != []:
                name = data[0]
                email = data[1][len('E-mail: ') + 1:]
                webpage, cv, address, research = self.process_data(data, div)

                prof = {
                    'name': name,
                    'email': email,
                    'webpage': webpage,
                    'page_ok': '',
                    'address': address,
                    'cv': cv,
                    'research': research
                }

                if prof['webpage']:
                    self.logger.info("KURVA ANYAD")
                    yield scrapy.Request(
                        prof['webpage'],
                        callback=self.parse_httpbin,
                        cb_kwargs={'prof': prof},
                        errback=self.errback_httpbin,
                        dont_filter=True
                    )
                else:
                    yield prof

        # Oneletrajzok es kepek letoltese
        yield {
            'file_urls': clean_cv_urls,
            'image_urls': clean_img_urls,
        }
